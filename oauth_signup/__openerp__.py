# -*- coding: utf-8 -*-
#
#    Copyright (C) 17/12/2014  Jose J. Duran, cavefish@gmail.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
{
    'name': "OAuth sign-up",
    'version': '1.0',
    'depends': ['base', 'auth_oauth'],
    'author': "Jose J. Duran",
    'category': 'OAuth',
    'description': """
    This modules makes it possible to sign-up with OAuth
    """,
    # data files always loaded at installation
    'data': [
        'auth_oauth_data.xml',
    ],
    # data files containing optionally loaded demonstration data
    'demo': [
    ],
}
