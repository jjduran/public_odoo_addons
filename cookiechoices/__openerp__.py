# -*- coding: utf-8 -*-
#
#    Copyright (C) 17/12/2014  Jose J. Duran, cavefish@gmail.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
{
    'name': "Cookiechoices",
    'version': '1.0',
    'depends': ['website'],
    'author': "Jose J. Duran",
    'category': 'web',
    'description': """
    This modules makes it possible to invoke the cookichoices.js script

    Example: Edit homepage, and add something like this:
    
<?xml version="1.0"?>
<t name="Homepage" priority="29" t-name="website.homepage">
    <t t-call="website.layout">
      <div id="wrap" class="oe_structure oe_empty"/>
      <script src='cookiechoices/static/cookiechoices.js'/>
      <script>
document.addEventListener('DOMContentLoaded', function(event) {
    cookieChoices.showCookieConsentBar('Your message for visitors here',
      'close message', 'learn more', 'http://example.com');
  });
</script>
    </t>
</t>

    """,
    # data files always loaded at installation
    'data': [
    ],
    # data files containing optionally loaded demonstration data
    'demo': [
    ],
}
